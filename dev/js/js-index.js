
+$(document).ready(function () {
    var el = $('.carrot');
    var originalelpos = el.offset().top; // take it where it originally is on the page


    $(window).scroll(function () {
        var el = $('.carrot'); // important! (local)
        var elpos = el.offset().top; // take current situation
        var windowpos = $(window).scrollTop();
        var finaldestination = windowpos + originalelpos + 10000;
        el.stop().animate({ 'top': finaldestination }, 150000);
    });
});


